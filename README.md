Role Name
=========

AutoLogic Technology's OS update role, useful for updating all packages on a fresh VM or Cloud based system, which often are pre-computed or gold images created weeks if not months prior to being used.

License
-------

BSD

Author Information
------------------

- Michael Crilly
- AutoLogic Technology Ltd
- http://www.mcrilly.me/
